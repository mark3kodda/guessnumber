package main.java.com.GuessNumber.service;

import main.java.com.GuessNumber.memory.GenerateRandom;
import java.util.Scanner;

class AnsweringBureauService {

    private GenerateRandom searchPool;
    private final Scanner scanner = new Scanner(System.in);
    private boolean answerIfCorrect = false;
    private int targetedNumber;
    private int timesToGuessInit;
    private int hints = 0;
    private final static String EXIT = "exit";

    private boolean correctingMSG = true;

    void menuLoop(){
        String choice;
            searchPool = null;
            choice = scanner.nextLine();
            switch (choice) {
                case "start":
                    start();
                    break;
                case "help":
                    help();
                    break;
                case "hints":
                    hints();
                    break;
                case "rules":
                    rules();
                    break;
                case EXIT:
                    System.exit(0);
                    return;
                default:
                    if(correctingMSG){
                        System.out.println("Invalid menu choice. Use 'help' if needed.");
                    }
                    correctingMSG=true;
                    break;
            }
    }


    private void help(){
        System.out.println("List of commands:\n start \n help \n rules \n hints \n exit ");
    }

    private void hints(){

        System.out.println("Do you want to enable the hints?(yes/no)");
        String yes = "yes";
        String no = "no";
        String answer = scanner.nextLine();
        if(answer.equalsIgnoreCase(no)){
            hints = 0;
            System.out.println("Hints are deactivated ");
        }else if(answer.equalsIgnoreCase(yes)){
            hints = 1;
            System.out.println("Hints are activated ");
        }else {
            System.out.println("Invalid command on Hints. Out to main menu");
        }
    }

    private void rules(){
        System.out.println("Enter 'start' to set the search pool");
        System.out.println("START line < END line. Range must be set in INT type");
        System.out.println("START must be equal or greater than 1");
        System.out.println("END must be equal or lesser than 200");
        System.out.println("Number of guesses can range from 1 to 15");

    }

    private void start(){
        settingSearchPool();
        if(searchPool!=null){
            gameLoop();
        }
    }

    private void gameLoop(){
    do {
            checkIfOutOfGuesses();
            String command = scanner.next();
            checkIfThereINT(command);
        }while (!answerIfCorrect);
    }

    private void settingSearchPool(){
        System.out.println("Please enter: \n -START line \n -END line \n -number of guesses"); // три числа

        try{

        int startLine = scanner.nextInt();
        int endLine = scanner.nextInt();
        int timesToTry = scanner.nextInt();

        if(startLine < 201 && endLine < 201 && startLine> 0 && endLine > 0 && startLine != endLine
        && 0 < timesToTry && timesToTry < 16){
            searchPool = new GenerateRandom(startLine,endLine,timesToTry);
        }else{
            System.out.println("There was error at setting search pool constructor. Please read rules. Out to main menu.");
            searchPool = null;
            correctingMSG=false;
            return; }
        }
        catch (Exception e)
        { //вроде сюда и не доходим теперь никогда
            System.out.println("There was error at setting search pool constructor. Please read rules. Out to main menu.");
            searchPool=null;
            correctingMSG=false;
            return;

        }

        System.out.println("Hi, I have a secret number ranged from "+searchPool.getStart()+" to "+searchPool.getEnd()+"." +
                " Try to pick it with "+searchPool.getTimesToGuess()+" guesses!");
        timesToGuessInit =searchPool.getTimesToGuess();
        targetedNumber = searchPool.getNextRandom();
    }

    private void checkIfThereINT(String command){
            try{
                int currentGivenNumber = Integer.parseInt(command);
                searchPool.setGivenAnswer(currentGivenNumber);
                searchPool.setTimesToGuess(searchPool.getTimesToGuess()-1);
                compareWithPreviousAnswerAndTargetedNumber(currentGivenNumber);
                searchPool.setPreviousGuess(searchPool.getGivenAnswer());

            }
            catch (/*NumberFormatException*/NumberFormatException e){
                if(command.equalsIgnoreCase(EXIT)){
                    System.out.println("Show down");
                    System.exit(0);
                }else{
                    System.out.println(command);
                    System.out.println("Enter INT to guess or 'exit' ");
                }
            }
    }

    private void compareWithPreviousAnswerAndTargetedNumber(int givenAnswer){
        if(givenAnswer == targetedNumber){
            System.out.println("Congratulations!!! You have guessed number ("+targetedNumber+") at "+(timesToGuessInit -searchPool.getTimesToGuess())+" try!");
            answerIfCorrect = true;
            System.exit(0);
            return;
        }
        if(hints==1){
            if(searchPool.getPreviousGuess()!=0){
                System.out.println("Your previous guess was "+searchPool.getPreviousGuess()+"");
                System.out.print("Delta: ");
                System.out.println("was "+Math.abs(targetedNumber-searchPool.getPreviousGuess())+" and became "+Math.abs(targetedNumber-givenAnswer)+"");
            }
        }
        if(searchPool.getPreviousGuess()==0){
            System.out.println("Your first answer "+givenAnswer);
        }

        if(searchPool.getPreviousGuess()!=0){ // когда уже есть предыдущий ответ
            if(Math.abs(targetedNumber-givenAnswer) == Math.abs(targetedNumber-searchPool.getPreviousGuess()))
            {
                System.out.println("Same distance. "+searchPool.getTimesToGuess()+" guesses left");
            }

            if(Math.abs(targetedNumber-givenAnswer) > Math.abs(targetedNumber-searchPool.getPreviousGuess())) //если расстояние от целевого до данного, от целевого до предидущего          как привязать предидущее число
            {
                System.out.println("Colder… "+searchPool.getTimesToGuess()+" guesses left");
            }

            if(Math.abs(targetedNumber-givenAnswer) < Math.abs(targetedNumber-searchPool.getPreviousGuess()))
            {
                System.out.println("Warmer! "+searchPool.getTimesToGuess()+" guesses left");
            }
        }
    }

    private void checkIfOutOfGuesses(){
            if(searchPool.getTimesToGuess()==0){
                System.out.print("You have run out of guesses. RIP. ");
                System.out.println("Secret number was "+ targetedNumber);
            }
    }
}
