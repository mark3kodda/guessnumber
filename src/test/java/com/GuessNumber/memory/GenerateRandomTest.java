package test.java.com.GuessNumber.memory;

import main.java.com.GuessNumber.memory.GenerateRandom;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

class GenerateRandomTest {

    static Arguments[] getNextRandomArgs(){
        return new Arguments[]{

                Arguments.arguments(10,20),
                Arguments.arguments(2,10),
                Arguments.arguments(3,55)
        };
    }

    @ParameterizedTest
    @MethodSource("getNextRandomArgs")
    void getNextRandom(int start,int end){
        GenerateRandom cut = new GenerateRandom(start,end);
        int expected = cut.getNextRandom();
        System.out.println(expected);

        Assertions.assertTrue(start < expected & expected < end);
    }
}